db.createUser(
    {
        user: "andrea",
        pwd: "12345",
        roles: [
            {
                role: "readWrite",
                db: "weather_db"
            }
        ]
    }
);


db.createCollection("data");

db.data.insertOne({
    "temperature":35.5, 
    "feels_like":40,
    "presure":1016,
    "humidity":16
});

db.data.insertMany([
    {
        "temperature":5, 
        "feels_like":2,
        "presure":564,
        "humidity":8
    },
     {
        "temperature":25.5, 
        "feels_like":20,
        "presure":1496,
        "humidity":58
    },
     {
        "temperature":45, 
        "feels_like":50,
        "presure":565,
        "humidity":0.84
    }
]);

db.data.find();
    
